<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use dcastello\TraventyBundle\Entity\Activity;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadActivityData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{

    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
    
    public function getOrder()
    {
        return 30;
    }

    public function load(ObjectManager $manager)
    {
        $castellonCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Castellón de la Plana");
        $valenciaCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Valencia");
        $peniscolaCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Peñiscola");
        
        $cities = array($castellonCity,$peniscolaCity,$valenciaCity);        
        
        for ($index = 1; $index < 100; $index++) {
            
            $randomCity = rand(0, 2);
            $activity = new Activity();
            $activity->setName("Activity $index");
            $activity->setCity($cities[$randomCity]);
            $activity->setCountry("Spain");
            $activity->setDate($this->generateRandomDate());
            $activity->setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in leo vulputate tortor facilisis eleifend et sit amet massa. Morbi vel libero vel augue fermentum malesuada. Cras ac tortor nec quam sollicitudin tempor. Proin eget mauris purus. Phasellus consequat nulla et magna gravida sit amet euismod sapien laoreet. Aenean aliquet congue purus, vel adipiscing mi pharetra vel. Mauris erat magna, pharetra at adipiscing id, aliquam eget ante. Nullam quis molestie magna. Nunc eu massa ac urna pharetra pellentesque. Aenean nisi massa, iaculis non lacinia a, convallis at ipsum. Vestibulum vehicula, nisi ut imperdiet interdum, metus sem tempor erat, ac ullamcorper ipsum magna nec sapien. Cras vel mi ipsum. Suspendisse sit amet dolor nisi.");

            $manager->persist($activity);
        }

        $manager->flush();
    }

    private function generateRandomDate()
    {
        return new \DateTime('now + ' . rand(10, 200) . ' days');
    }

}
