<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use dcastello\TraventyBundle\Entity\Travel;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

class LoadTravelData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 60;
    }

    public function load(ObjectManager $manager)
    {
        $activities = $manager->getRepository("TraventyBundle:Activity")->findAll();
        $resources = $manager->getRepository("TraventyBundle:Resource")->findAll();
        $agencies = $manager->getRepository("TraventyBundle:Agency")->findAll();

        foreach ($agencies as $agency) {
            $totalTravels = rand(2, 5);

            for ($index = 1; $index <= $totalTravels; $index++) {
                $travel = new Travel();
                $travel->setName("Travel " . $agency->getId() . $index);

                $total = rand(2, 5);
                $auxNum = 1;
                foreach ($activities as $activity) {
                    if ($auxNum == $total) {
                        break;
                    }
                    $travel->addActivity($activity);
                    $auxNum++;
                }

                $total = rand(2, 5);
                $auxNum = 1;
                foreach ($resources as $resource) {
                    if ($auxNum == $total) {
                        break;
                    }
                    $travel->addResource($resource);
                    $auxNum++;
                }

                $travel->setAttendantLimit(rand(1, 20));
                $travel->setDate(new \DateTime('now + ' . rand(30, 200) . ' days'));

                $agency->addTravel($travel);
                $manager->persist($agency);
                $manager->flush();

                $this->manageAcl($travel, $agency, MaskBuilder::MASK_OWNER);
            }
        }
    }

    private function manageAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->container->get('security.acl.provider');

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

}
