<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCityDataFromFile extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 2;
    }

    public function load(ObjectManager $manager)
    {
        $self = $this;

        $this->container->get('lexik_fixtures_mapper.loader.csv')
            ->load(sprintf('%s/data/city.csv', __DIR__))
            ->setEntityName('\\dcastello\\TraventyBundle\\Entity\\City')
            ->mapColumn(0, 'id')
            ->mapColumn(1, function($data, &$object) use ($self) {
                $state = $self->getReference('state-'.$data);
                $object->setState($state);
            })
            ->mapColumn(2, 'name')
            ->mapColumn(3, 'slug')
            ->mapColumn(4, 'latitude')
            ->mapColumn(5, 'longitude')
            ->mapColumn(6, 'postal_code')  
            ->persist()
        ;
    }

}
