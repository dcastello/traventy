<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadStateDataFromFile extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 1;
    }

    public function load(ObjectManager $manager)
    {
        $self = $this;

        $this->container->get('lexik_fixtures_mapper.loader.csv')
            ->load(sprintf('%s/data/state.csv', __DIR__))
            ->setEntityName('\\dcastello\\TraventyBundle\\Entity\\State')
            ->mapColumn(0, function($data, &$object) use ($self) {
                    $self->addReference('state-'.$data, $object);
                })
            ->mapColumn(1, 'name')
            ->mapColumn(2, 'slug')
            ->persist()
        ;
    }

}
