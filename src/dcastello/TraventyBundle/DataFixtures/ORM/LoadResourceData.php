<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use dcastello\TraventyBundle\Entity\Resource;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadResourceData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;
    
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 40;
    }

    public function load(ObjectManager $manager)
    {
        $castellonCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Castellón de la Plana");
        $valenciaCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Valencia");
        $peniscolaCity = $manager->getRepository("TraventyBundle:City")->findOneByName("Peñiscola");
        
        $cities = array($castellonCity,$peniscolaCity,$valenciaCity);        
                
        for ($index = 1; $index < 100; $index++) {
            
            $randomCity = rand(0, 2);
            $resource = new Resource();
            $resource->setName("Resource $index");
            $resource->setCity($cities[$randomCity]);
            $resource->setCountry("Spain");
            $resource->setTelephone("987654321");
            $resource->setFax("123456789");
            $resource->setWeb("http://www.symfony.es");

            $manager->persist($resource);
        }
        $manager->flush();
    }
}
