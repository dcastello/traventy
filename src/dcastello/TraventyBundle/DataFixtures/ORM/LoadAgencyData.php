<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use dcastello\TraventyBundle\Entity\Agency;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class LoadAgencyData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 20;
    }

    public function load(ObjectManager $manager)
    {
        for ($index = 1; $index < 10; $index++) {
            $agency = new Agency();
            $agency->setUsername("agency$index");
            $agency->setName("Agency $index");
            $agency->setCif($this->getRandomCif());
            $agency->setWeb("http://usuario$index");
            $agency->setCity("La city $index");
            $agency->setEmail("agency$index@localhost");
            $agency->setSalt(md5(time()));

            $passwordTextPlane = "agency$index";

            $encoder = $this->container->get('security.encoder_factory')->getEncoder($agency);

            $agency->setPassword($encoder->encodePassword($passwordTextPlane, $agency->getSalt()));

            $manager->persist($agency);
            $manager->flush();

            $this->manageAcl($agency, $agency, MaskBuilder::MASK_OWNER);
        }
    }

    private function manageAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->container->get('security.acl.provider');

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

    private function getRandomCif()
    {
        return rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9) . rand(0, 9);
    }

}
