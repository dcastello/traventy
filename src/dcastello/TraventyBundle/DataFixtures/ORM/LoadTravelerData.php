<?php

namespace dcastello\TraventyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use dcastello\TraventyBundle\Entity\Traveler;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

class LoadTravelerData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getOrder()
    {
        return 10;
    }

    public function load(ObjectManager $manager)
    {
        for ($index = 1; $index < 10; $index++) {
            $traveler = new Traveler();
            $traveler->setUsername("traveler$index");
            $traveler->setName("Traveler $index");
            $traveler->setSurname("Surname user $index");
            $traveler->setWeb("http://usuario$index");
            $traveler->setCity("La city $index");
            $traveler->setEmail("traveler$index@localhost");
            $traveler->setSalt(md5(time()));

            $passwordTextPlane = "traveler$index";

            $encoder = $this->container->get('security.encoder_factory')->getEncoder($traveler);

            $traveler->setPassword($encoder->encodePassword($passwordTextPlane, $traveler->getSalt()));

            $manager->persist($traveler);
            $manager->flush();

            $this->manageAcl($traveler, $traveler, MaskBuilder::MASK_OWNER);
        }
    }

    private function manageAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->container->get('security.acl.provider');

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

}
