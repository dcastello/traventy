<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;

class DefaultController extends Controller
{

    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $numberTopElements = 4;
        
        return array(
            'topActivities' => $em->getRepository('TraventyBundle:Activity')->findTop($numberTopElements),
            'topResources'  => $em->getRepository('TraventyBundle:Resource')->findTop($numberTopElements),
            'topExcursions' => $em->getRepository('TraventyBundle:Excursion')->findTop($numberTopElements),
            'topTravels'    => $em->getRepository('TraventyBundle:Travel')->findTop($numberTopElements),
        );
    }

    /**
     * @Template()
     */
    public function loginAction()
    {
        $request = $this->getRequest();
        $session = $request->getSession();

        $error = $request->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR, $session->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        return array(
          'last_username' => $session->get(SecurityContext::LAST_USERNAME),
          'error' => $error
        );
    }

}
