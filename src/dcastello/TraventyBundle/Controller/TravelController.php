<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use dcastello\TraventyBundle\Entity\Agency;
use dcastello\TraventyBundle\Entity\Travel;
use dcastello\TraventyBundle\Entity\TravelSubscription;
use dcastello\TraventyBundle\Entity\Exception\AttendantLimitExceededException;
use dcastello\TraventyBundle\Entity\Exception\DuplicatedSubscriptionException;
use dcastello\TraventyBundle\Entity\Exception\SubscriptionNotExistException;

class TravelController extends Controller
{

    /**
     * @Route("/travels", name="travels")
     * @Method("GET")
     * @Template("TraventyBundle:Travel:travels.html.twig")
     */
    public function getTravelsAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $travels = $em->getRepository("TraventyBundle:Travel")->findBy(array('active' => true));

        return array(
            'travels' => $travels
        );
    }

    /**
     * @Route("/agency/{username}/travel/{travelid}/subscribe", name="agency_travel_add_attendant")
     * @Method("GET")
     */
    public function createSubscriptionAction($username, $travelid)
    {
        $securityContext = $this->get("security.context");
        $traveler = $securityContext->getToken()->getUser();

        if ($securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException;
        }

        $entityManager = $this->getDoctrine()->getEntityManager();

        if (!$travel = $entityManager->getRepository("TraventyBundle:Travel")->find($travelid)) {
            return $this->createNotFoundException("Travel not exists");
        }

        try {
            $travelSubscriptionManager = $this->get("traventy.manager.subscription");
            $travelSubscriptionManager->createSubscription($travel, $traveler);

            $alertMessage = array("title" => "Well done!", "message" => "Your subscription is pending to accept.");
            $this->getRequest()->getSession()->setFlash("success", $alertMessage);
        } catch (AttendantLimitExceededException $e) {
            $alertMessage = array("title" => "Warning!", "message" => $e->getMessage());
            $this->getRequest()->getSession()->setFlash("warning", $alertMessage);
        } catch (DuplicatedSubscriptionException $e) {
            $alertMessage = array("title" => "Information", "message" => $e->getMessage());
            $this->getRequest()->getSession()->setFlash("info", $alertMessage);
        }

        return $this->redirect($this->generateUrl('agency_detail_travel', array('id' => $travelid, 'username' => $username)));
    }
    
    /**
     * @Route("/agency/{username}/travel/{travelid}/subscription/{travelerusername}/accept", name="travel_subscription_accept")
     * @Method("GET")
     */
    public function acceptSubscriptionAction($username,$travelid,$travelerusername)
    {
        $securityContext = $this->get("security.context");
        
        if (!$securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException;
        }
        
        $entityManager = $this->getDoctrine()->getEntityManager();
        
        if (!$travel = $entityManager->getRepository("TraventyBundle:Travel")->find($travelid)) {
            return $this->createNotFoundException("Travel not exists");
        }
        
        if (!$traveler = $entityManager->getRepository("TraventyBundle:Traveler")->findOneByUsername($travelerusername)) {
            return $this->createNotFoundException("Traveler not exists");
        }
                
        $travelerSubscriptionManager = $this->get("traventy.manager.subscription");
        
        try {
            $travelerSubscriptionManager->acceptSubscription($travel,$traveler);
            
            $alertMessage = array("title" => "Well done!", "message" => "You have been subscribed to the travel.");
            $this->getRequest()->getSession()->setFlash("success", $alertMessage);
            
        } catch (SubscriptionNotExistException $exception) {
            $alertMessage = array("title" => "Error!", "message" => $exception->getMessage());
            $this->getRequest()->getSession()->setFlash("error", $alertMessage);
        }
        
        return $this->redirect($this->generateUrl('agency_show_travel', array('id' => $travelid, 'username' => $username)));
            
    }
    
    /**
     * @Route("/agency/{username}/travel/{travelid}/subscription/{travelerusername}/deny", name="travel_subscription_deny")
     * @Method("GET")
     */
    public function denySubscriptionActioAction($username,$travelid,$travelerusername)
    {
        $securityContext = $this->get("security.context");
        $agency = $securityContext->getToken()->getUser();
        
        if (!$securityContext->isGranted('OWNER','ROLE_AGENCY')) {
            throw new AccessDeniedException;
        }
        
        $entityManager = $this->getDoctrine()->getEntityManager();
        
        if (!$travel = $entityManager->getRepository("TraventyBundle:Travel")->find($travelid)) {
            return $this->createNotFoundException("Travel not exists");
        }
        
        if (!$traveler = $entityManager->getRepository("TraventyBundle:Traveler")->findOneByUsername($travelerusername)) {
            return $this->createNotFoundException("Traveler not exists");
        }
                
        $travelerSubscriptionManager = $this->get("traventy.manager.subscription");
        
        try {
            $travelerSubscriptionManager->denySubscription($travel,$traveler);
            
            $alertMessage = array("title" => "Well done!", "message" => "$traveler has been unsubscribed.");
            $this->getRequest()->getSession()->setFlash("success", $alertMessage);
            
        } catch (SubscriptionNotExistException $exception) {
            $alertMessage = array("title" => "Error!", "message" => $exception->getMessage());
            $this->getRequest()->getSession()->setFlash("error", $alertMessage);
        }
        
        return $this->redirect($this->generateUrl('agency_show_travel', array('id' => $travelid, 'username' => $username)));
            
        
    }    

    /**
     * @Route("/agency/{username}/travel/whithout-activity/{activityid}", name="travel_whithoutactivity")
     * @Method("GET")
     */
    public function getTravelsWithoutActivityAction($activityid)
    {
        $securityContext = $this->get('security.context');
        $agency = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException;
        }

        $em = $this->getDoctrine()->getEntityManager();
        $travels = $em->getRepository("TraventyBundle:Travel")->findTravelsWithoutActivity($agency->getId(), $activityid);

        $result = array();
        foreach ($travels as $travel) {
            $result[$travel->getId()] = $travel->getName();
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/agency/{username}/travel/whithout-resource/{resourceid}", name="travel_whithoutresource")
     * @Method("GET")
     */
    public function getExcursionsWithoutResourceAction($resourceid)
    {
        $securityContext = $this->get('security.context');
        $agency = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException;
        }

        $em = $this->getDoctrine()->getEntityManager();
        $travels = $em->getRepository("TraventyBundle:Travel")->findExcursionsWithoutResource($agency->getId(), $resourceid);

        $result = array();
        foreach ($travels as $travel) {
            $result[$travel->getId()] = $travel->getName();
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/{travelid}/activity/add/{activityid}", name="travel_add_activity", options={"expose" = true})
     * @Method("GET")
     */
    public function addActivityTravelAction($travelid, $activityid)
    {
        $securityContext = $this->get('security.context');
        $agency = $securityContext->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($activityid);
        $travel = $em->getRepository('TraventyBundle:Travel')->find($travelid);

        if ($travel == null) {
            $this->createNotFoundException("Not travel found");
        }

        if ($activity == null) {
            $this->createNotFoundException("Not activity found");
        }

        if (false === $securityContext->isGranted('OWNER', $travel)) {
            throw new AccessDeniedException();
        }

        $travel->addActivity($activity);
        $em->persist($travel);
        $em->flush();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $result = array("success" => true, "message" => "Activity added to " . $travel->getName());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return $this->redirect($this->getRequest()->headers->get('referer'));
        }
    }

    /**
     * @Route("/{travelid}/resource/add/{resourceid}", name="travel_add_resource", options={"expose" = true})
     * @Method("GET")
     */
    public function addResourceTravelAction($travelid, $resourceid)
    {
        $securityContext = $this->get('security.context');
        $agency = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($resourceid);
        $travel = $em->getRepository('TraventyBundle:Travel')->find($travelid);

        if ($travel == null) {
            $this->createNotFoundException("Not travel found");
        }

        if ($resource == null) {
            $this->createNotFoundException("Not resource found");
        }

        if (false === $securityContext->isGranted('OWNER', $travel)) {
            throw new AccessDeniedException();
        }

        $travel->addResource($resource);
        $em->persist($travel);
        $em->flush();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $result = array("success" => true, "message" => "Resource added to " . $travel->getName());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return $this->redirect($this->getRequest()->headers->get('referer'));
        }
    }

    /**
     * @Route("/{travelid}/activity/remove/{activityid}", name="travel_remove_activity")
     * @Method("GET")
     */
    public function removeActivityTravelAction($travelid, $activityid)
    {
        $securityContext = $this->get('security.context');

        if (false === $securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($activityid);
        $travel = $em->getRepository('TraventyBundle:Travel')->find($travelid);

        if ($travel == null) {
            $this->createNotFoundException("Not travel found");
        }

        if ($activity == null) {
            $this->createNotFoundException("Not activity found");
        }

        if (false === $securityContext->isGranted('OWNER', $travel)) {
            throw new AccessDeniedException();
        }

        $travel->getActivities()->removeElement($activity);
        $em->persist($travel);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{travelid}/resource/remove/{resourceid}", name="travel_remove_resource")
     * @Method("GET")
     */
    public function removeResourceTravelAction($travelid, $resourceid)
    {
        $securityContext = $this->get('security.context');

        if (false === $securityContext->isGranted('ROLE_AGENCY')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($resourceid);
        $travel = $em->getRepository('TraventyBundle:Travel')->find($travelid);

        if ($travel == null) {
            $this->createNotFoundException("Not travel found");
        }

        if ($resource == null) {
            $this->createNotFoundException("Not resource found");
        }

        if (false === $securityContext->isGranted('OWNER', $travel)) {
            throw new AccessDeniedException();
        }

        $travel->getResources()->removeElement($resource);
        $em->persist($travel);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

}
