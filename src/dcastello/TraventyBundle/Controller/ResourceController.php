<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JMS\SecurityExtraBundle\Annotation\Secure;
use dcastello\TraventyBundle\Form\ResourceSearchType;
use dcastello\TraventyBundle\Entity\Resource;
use dcastello\TraventyBundle\Form\ResourceType;

class ResourceController extends Controller
{

    /**
     * List all Resource entities
     * 
     * @Route("/resource", name="resource")
     * @Method("GET") 
     * @Template()
     */
    public function indexAction()
    {
        $securityContext = $this->get('security.context');
        $em = $this->getDoctrine()->getEntityManager();

        $favoritesResources = null;
        if (true === $securityContext->isGranted('ROLE_TRAVELER')) {
            $user = $securityContext->getToken()->getUser();
            $favoritesResources = $em->getRepository('TraventyBundle:Traveler')->findFavoritesResourcesId($user->getId());
        }

        $criteria = $this->generateCriteriaFromQueryParams();
        $entities = $em->getRepository('TraventyBundle:Resource')->findBy($criteria);

        $searchResourceForm = $this->createForm(new ResourceSearchType());
        $searchResourceForm->bindRequest($this->getRequest());

        return array(
            'entities' => $entities,
            'favoritesResources' => $favoritesResources,
            'searchForm' => $searchResourceForm->createView()
        );
    }

    private function generateCriteriaFromQueryParams()
    {
        $criteria = array();
        $request = $this->getRequest();

        if ($request->query->has('resourcesearchtype')) {
            $params = $request->query->get('resourcesearchtype');
            if (key_exists("name", $params) && !empty($params['name'])) {
                $criteria['name'] = $params['name'];
            }
            if (key_exists("city", $params) && !empty($params['city'])) {
                $criteria['city'] = $params['city'];
            }
        }

        return $criteria;
    }

    /**
     * Finds and displays a Resource entity
     * 
     * @Route("/resource/{id}/show",name="resource_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('TraventyBundle:Resource')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resource entity');
        }

        return array(
            'entity' => $entity
        );
    }

    /**
     * Displays a form to create a new Resource entity.
     *
     * @Route("/admin/resource/new", name="admin_resource_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Resource();
        $form = $this->createForm(new ResourceType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Resource entity.
     *
     * @Route("/admin/resource/create", name="admin_resource_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("TraventyBundle:Resource:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Resource();
        $request = $this->getRequest();
        $form = $this->createForm(new ResourceType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Resource created.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('admin_resource', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Resource entity.
     *
     * @Route("/admin/resource/{id}/edit", name="admin_resource_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('TraventyBundle:Resource')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resource entity.');
        }

        $editForm = $this->createForm(new ResourceType(), $entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edits an existing Resource entity.
     *
     * @Route("/admin/resource/{id}/update", name="admin_resource_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("TraventyBundle:Resource:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('TraventyBundle:Resource')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resource entity.');
        }

        $editForm = $this->createForm(new ResourceType(), $entity);

        $request = $this->getRequest();

        $editForm->bindRequest($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Resource updated.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('admin_resource_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Deletes a Resource entity.
     *
     * @Route("/admin/resource/{id}/delete", name="admin_resource_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $entity = $entityManager->getRepository('TraventyBundle:Resource')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Resource entity.');
        }

        try {
            $entityManager->remove($entity);
            $entityManager->flush();

            $alert = array("title" => "Well done!", "message" => "Resource deleted.");
            $flashType = "success";
        } catch (\Exception $exc) {
            $flashType = "error";
            if ($exc->getCode() == '23000') {
                $alert = array("title" => "Error!", "message" => 'The Resource is being used in some Travels or Excursions. You can not delete it.');
            } else {
                $alert = array("title" => "Error!", "message" => 'Internals problems. Can you try it later?');
            }
        }

        $this->getRequest()->getSession()->setFlash($flashType, $alert);

        return $this->redirect($this->generateUrl('admin_resource'));
    }

}
?>
