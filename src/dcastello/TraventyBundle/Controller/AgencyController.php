<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use dcastello\TraventyBundle\Entity\Agency;
use dcastello\TraventyBundle\Entity\Travel;
use dcastello\TraventyBundle\Form\AgencyBasicInfoType;
use dcastello\TraventyBundle\Form\AgencyAccountInfoType;
use dcastello\TraventyBundle\Form\AgencySocialNetworkLinkType;
use dcastello\TraventyBundle\Form\TravelType;

/**
 * 
 * Agency controller
 * 
 * @Route("/agency")
 */
class AgencyController extends Controller
{

    /**
     * @Route("/{username}/travels", name="agency_travels")
     * @Template("TraventyBundle:Agency:travels.html.twig")
     */
    public function getTravelsAction()
    {
        $agency = $this->get('security.context')->getToken()->getUser();

        $this->getRequest()->getSession()->set("menuBackend", "travel");
        return array(
            'travels' => $agency->getTravels()
        );
    }

    /**
     * @Route("/{username}/travel/{id}/detail", name="agency_detail_travel")
     * @Template("TraventyBundle:Travel:detailTravel.html.twig")
     */
    public function detailTravelAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $travel = $em->getRepository("TraventyBundle:Travel")->find($id);
        $acceptedSubscriptions = $em->getRepository("TraventyBundle:TravelSubscription")->findTravelersWithAcceptedSubscriptionsByTravel($travel->getId());

        return array(
            'travel' => $travel,
            'acceptedSubscriptions' => $acceptedSubscriptions
        );
    }

    /**
     * @Route("/{username}/travel/{id}/show", name="agency_show_travel")
     * @Template("TraventyBundle:Travel:showTravel.html.twig")
     */
    public function showTravelAction($id)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $travel = $entityManager->getRepository("TraventyBundle:Travel")->find($id);

        if (false === $this->get('security.context')->isGranted('VIEW', $travel)) {
            throw new AccessDeniedException;
        }

        $pendingSubscriptions = $entityManager->getRepository("TraventyBundle:TravelSubscription")->findPendingSubscriptionsByTravel($travel->getId());
        $acceptedSubscriptions = $entityManager->getRepository("TraventyBundle:TravelSubscription")->findAcceptedSubscriptionsByTravel($travel->getId());
        $deniedSubscriptions = $entityManager->getRepository("TraventyBundle:TravelSubscription")->findDeniedSubscriptionsByTravel($travel->getId());

        return array(
            'travel' => $travel,
            'pendingSubscriptions' => $pendingSubscriptions,
            'acceptedSubscriptions' => $acceptedSubscriptions,
            'deniedSubscriptions' => $deniedSubscriptions
        );
    }

    /**
     * @Route("/{username}/travel/new", name="agency_new_travel")
     * @Template("TraventyBundle:Travel:newTravel.html.twig")
     */
    public function newTravelAction()
    {
        $travelForm = $this->createForm(new TravelType(), new Travel());

        if (false === $this->get('security.context')->isGranted('CREATE')) {
            throw new AccessDeniedException;
        }

        $this->getRequest()->getSession()->set("menuBackend", "travel");
        return array(
            'update' => false,
            'form' => $travelForm->createView()
        );
    }

    /**
     * @Route("/{username}/travel/create", name="agency_create_travel")
     * @Method("POST")
     * @Template("TraventyBundle:Travel:newTravel.html.twig")
     */
    public function createTravelAction()
    {
        $agency = $this->get("security.context")->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $travel = new Travel();
        $travelForm = $this->createForm(new TravelType(), $travel);

        if (false === $this->get('security.context')->isGranted('CREATE')) {
            throw new AccessDeniedException;
        }

        $travelForm->bindRequest($this->getRequest());

        if ($travelForm->isValid()) {
            $agency->addTravel($travel);
            $em->persist($agency);
            $em->flush();

            $this->insertOrUpdateAcl($travel, $agency, MaskBuilder::MASK_OWNER);

            $alert = array("title" => "Well done!", "message" => "New travel created.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('agency_travels', array('username' => $agency->getUsername())));
        }

        return array(
            'update' => false,
            'form' => $travelForm->createView()
        );
    }

    /**
     * @Route("/{username}/travel/{id}/delete",name="agency_delete_travel")
     * @Template("TraventyBundle:Agency:travels.html.twig")
     */
    public function deleteTravelAction($username, $id)
    {
        $agency = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $travel = $em->getRepository("TraventyBundle:Travel")->find($id);

        if (false === $this->get('security.context')->isGranted('DELETE', $travel)) {
            throw new AccessDeniedException;
        }

        $agency->removeTravel($travel);
        $em->persist($agency);
        $em->flush();

        $this->deleteAcl($travel);

        $alert = array("title" => "Well done!", "message" => "Travel deleted.");
        $this->getRequest()->getSession()->setFlash("success", $alert);

        return $this->redirect($this->generateUrl('agency_travels', array('username' => $agency->getUsername())));
    }

    /**
     * @Route("/{username}/travel/{id}/edit", name="agency_edit_travel")
     * @Template("TraventyBundle:Travel:newTravel.html.twig")
     */
    public function editTravelAction($username, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $travel = $em->getRepository("TraventyBundle:Travel")->find($id);

        if (false === $this->get('security.context')->isGranted('EDIT', $travel)) {
            throw new AccessDeniedException;
        }

        $form = $this->createForm(new TravelType(), $travel);

        return array(
            'id' => $id,
            'update' => true,
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/{username}/travel/{id}/update", name="agency_update_travel")
     * @Method("POST")
     * @Template("TraventyBundle:Travel:newTravel.html.twig")
     */
    public function updateTravelAction($username, $id)
    {
        $agency = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $travel = $em->getRepository("TraventyBundle:Travel")->find($id);

        if (false === $this->get('security.context')->isGranted('EDIT', $travel)) {
            throw new AccessDeniedException;
        }

        $travelForm = $this->createForm(new TravelType(), $travel);
        $travelForm->bindRequest($this->getRequest());

        if ($travelForm->isValid()) {
            $em->persist($travel);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Travel updated.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('agency_travels', array('username' => $agency->getUsername())));
        }

        return array(
            'id' => $id,
            'form' => $travelForm->createView(),
            'update' => true
        );
    }

    /**
     * @Route("/{username}", name="agency_home")
     * @Template()
     */
    public function indexAction($username)
    {
        $this->getRequest()->getSession()->remove("menuBackend");
        return array(
            'title' => 'Agency settings',
        );
    }

    /**
     * @Route("/{username}/edit", name="agency_edit")
     * @Template()
     */
    public function editAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $editForm = $this->createForm(new AgencyBasicInfoType(), $user);

        return array(
            'ruta' => 'agency_update',
            'title' => 'Basic info',
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * @Route("/{username}/update", name="agency_update")
     * @Method("POST")
     * @Template("TraventyBundle:Agency:edit.html.twig")
     */
    public function updateAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $editForm = $this->createForm(new AgencyBasicInfoType(), $user);
        $editForm->bindRequest($this->getRequest());

        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('agency_edit', array('username' => $user->getUsername())));
        }

        return array(
            'ruta' => 'agency_update',
            'title' => 'Basic info',
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edit account info
     * 
     * @Route("/{username}/account/edit", name="agency_account_edit")
     * @Template("TraventyBundle:Agency:edit.html.twig") 
     */
    public function editPasswordAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $editForm = $this->createForm(new AgencyAccountInfoType(), $user);

        return array(
            'ruta' => 'agency_account_update',
            'title' => 'Account info',
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Update account info
     *
     * @Route("/{username}/account/update", name="agency_account_update") 
     * @Method("POST")
     * @Template("TraventyBundle:Agency:edit.html.twig") 
     */
    public function updatePasswordAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $editForm = $this->createForm(new AgencyAccountInfoType(), $user);

        $originalPassword = $editForm->getData()->getPassword();
        $editForm->bindRequest($this->getRequest());

        if ($editForm->isValid()) {
            if (null == $user->getPassword()) {
                $user->setPassword($originalPassword);
            } else {
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $codedPassword = $encoder->encodePassword(
                        $user->getPassword(), $user->getSalt()
                );
            }
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('agency_account_edit', array('username' => $user->getUsername())));
        }

        return array(
            'ruta' => 'agency_account_update',
            'title' => 'Account info',
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edit social networks links
     * 
     * @Route("/{username}/network/edit", name="agency_network_edit")
     * @Template("TraventyBundle:Agency:edit.html.twig")
     */
    public function editSocialNetworkAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        $editForm = $this->createForm(new AgencySocialNetworkLinkType(), $user);

        return array(
            'ruta' => 'agency_network_update',
            'title' => 'Social network links',
            'edit_form' => $editForm->createView()
        );
    }

    /**
     * Update social network links
     * 
     * @Route("/{username}/network/update", name="agency_network_update")
     * @Method("POST")
     * @Template("TraventyBundle:Agency:edit.html.twig")
     */
    public function updateSocialNetworkAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $editForm = $this->createForm(new AgencySocialNetworkLinkType(), $user);

        $editForm->bindRequest($this->getRequest());
        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl('agency_network_edit', array('username' => $user->getUsername())));
        }

        return array(
            'ruta' => 'agency_network_update',
            'title' => 'Social network links',
            'edit_form' => $editForm->createView()
        );
    }

    private function insertOrUpdateAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->container->get('security.acl.provider');

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

    private function deleteAcl($object)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $this->get('security.acl.provider')->deleteAcl($idObject);
    }

}
