<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use dcastello\TraventyBundle\Entity\Activity;
use dcastello\TraventyBundle\Form\ActivityType;
use dcastello\TraventyBundle\Form\ActivitySearchType;

class ActivityController extends Controller
{

    /**
     * Lists all Activity entities.
     *
     * @Route("/activity", name="activity")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $securityContext = $this->get('security.context');
        $em = $this->getDoctrine()->getEntityManager();

        $favoritesActivities = null;
        if (true === $securityContext->isGranted('ROLE_TRAVELER')) {
            $user = $securityContext->getToken()->getUser();
            $favoritesActivities = $em->getRepository('TraventyBundle:Traveler')->findFavoritesActivitiesId($user->getId());
        }

        $criteria = $this->generateCriteriaFromQueryParams();
        $entities = $em->getRepository('TraventyBundle:Activity')->findBy($criteria);

        $searchActivityForm = $this->createForm(new ActivitySearchType());
        $searchActivityForm->bindRequest($this->getRequest());

        return array(
            'entities' => $entities,
            'favoritesActivities' => $favoritesActivities,
            'searchForm' => $searchActivityForm->createView()
        );
    }

    private function generateCriteriaFromQueryParams()
    {
        $criteria = array();
        $request = $this->getRequest();

        if ($request->query->has('activitysearchtype')) {
            $params = $request->query->get('activitysearchtype');
            if (key_exists("name", $params) && !empty($params['name'])) {
                $criteria['name'] = $params['name'];
            }
            if (key_exists("city", $params) && !empty($params['city'])) {
                $criteria['city'] = $params['city'];
            }
        }

        return $criteria;
    }

    /**
     * Finds and displays a Activity entity.
     *
     * @Route("/activity/{id}/show", name="activity_show")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('TraventyBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to create a new Activity entity.
     *
     * @Route("/admin/activity/new", name="admin_activity_new")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Activity();
        $form = $this->createForm(new ActivityType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a new Activity entity.
     *
     * @Route("/admin/activity/create", name="admin_activity_create")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("TraventyBundle:Activity:new.html.twig")
     */
    public function createAction()
    {
        $entity = new Activity();
        $request = $this->getRequest();
        $form = $this->createForm(new ActivityType(), $entity);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($entity);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Activity created.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('admin_activity', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Activity entity.
     *
     * @Route("/admin/activity/{id}/edit", name="admin_activity_edit")
     * @Secure(roles="ROLE_ADMIN")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $entity = $em->getRepository('TraventyBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        $editForm = $this->createForm(new ActivityType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $editForm->createView()
        );
    }

    /**
     * Edits an existing Activity entity.
     *
     * @Route("/admin/activity/{id}/update", name="admin_activity_update")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("POST")
     * @Template("TraventyBundle:Activity:edit.html.twig")
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $logger = $this->get('logger');

        $activity = $em->getRepository('TraventyBundle:Activity')->find($id);

        if (!$activity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

//        $originaImages = array();
//        foreach ($activity->getImages() as $image) {
//            $originaImages[] = $image;
//        }

        $editForm = $this->createForm(new ActivityType(), $activity);

        $editForm->bindRequest($this->getRequest());

        if ($editForm->isValid()) {

//            foreach ($activity->getImages() as $image) {                
//                foreach ($originaImages as $key => $toDel) {
//                    if ($toDel->getId() === $image->getId()) {
//                        unset($originaImages[$key]);
//                    }
//                }
//            }
//            
//            foreach ($originaImages as $image) {                
//                $activity->removeImage($image);
//            }

            $em->persist($activity);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Activity updated.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('admin_activity_edit', array('id' => $id)));
        }

        return array(
            'entity' => $activity,
            'form' => $editForm->createView()
        );
    }

    /**
     * Deletes a Activity entity.
     *
     * @Route("/admin/activity/{id}/delete", name="admin_activity_delete")
     * @Secure(roles="ROLE_ADMIN")
     * @Method("GET")
     */
    public function deleteAction($id)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $entity = $entityManager->getRepository('TraventyBundle:Activity')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Activity entity.');
        }

        try {
            $entityManager->remove($entity);
            $entityManager->flush();
            $alert = array("title" => "Well done!", "message" => "Activity deleted.");
            $flashType = "success";
        } catch (\Exception $exc) {
            $flashType = "error";
            if ($exc->getCode() == '23000') {
                $alert = array("title" => "Error!", "message" => 'The Activity is being used in some Travels or Excursions. You can not delete it.');
            } else {
                $alert = array("title" => "Error!", "message" => 'Internals problems. Can you try it later?');
            }
        }

        $this->getRequest()->getSession()->setFlash($flashType, $alert);

        return $this->redirect($this->generateUrl('admin_activity'));
    }

}
