<?php

/**
 * Description of AdminController
 *
 * @author david
 */

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{

    /**
     * @Route("/", name="admin_homepage")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("TraventyBundle:Admin:index.html.twig") 
     */
    public function dashboardAction()
    {
        $activityRepository = $this->getActivityRepository();
        $resourceRepository = $this->getResourceRepository();

        $stats = array(
            'numberActivities' => $activityRepository->numberTotalActivities(),
            'numberResources' => $resourceRepository->numberTotalResources()
        );

        return $stats;
    }

    /**
     * @Route("/activity", name="admin_activity")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("TraventyBundle:Admin:activities.html.twig") 
     */
    public function listActivitiesAction()
    {
        $paginator = $this->getPaginator();

        $activities = $paginator->paginate(
                        $this->getActivityRepository()->queryAllActivities()
                )->getResult();

        return array(
            'activities' => $activities,
            'paginator' => $paginator
        );
    }

    /**
     * @Route("/resource", name="admin_resource")
     * @Secure(roles="ROLE_ADMIN")
     * @Template("TraventyBundle:Admin:resources.html.twig") 
     */
    public function listResourcesAction()
    {
        $paginator = $this->getPaginator();

        $resources = $paginator->paginate(
                        $this->getResourceRepository()->queryAllResources()
                )->getResult();

        return array(
            'resources' => $resources,
            'paginator' => $paginator
        );
    }

    private function getPaginator()
    {
        $paginator = $this->get('ideup.simple_paginator');
        $paginator->setItemsPerPage(15);
        $paginator->setMaxPagerItems(5);
        return $paginator;
    }

    private function getActivityRepository()
    {
        return $this->getDoctrine()->getEntityManager()->getRepository("TraventyBundle:Activity");
    }

    private function getResourceRepository()
    {
        return $this->getDoctrine()->getEntityManager()->getRepository("TraventyBundle:Resource");
    }

Action}
