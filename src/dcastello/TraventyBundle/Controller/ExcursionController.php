<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use dcastello\TraventyBundle\Entity\Traveler;
use dcastello\TraventyBundle\Entity\Excursion;

class ExcursionController extends Controller
{

    /**
     * @Route("/excursions", name="excursions")
     * @Method("GET")
     * @Template("TraventyBundle:Excursion:excursions.html.twig")
     */
    public function getExcursionsAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $excursions = $em->getRepository("TraventyBundle:Excursion")->findBy(array('active' => true));
        
        return array(
            'excursions' => $excursions
        );
    }
    
    /**
     * @Route("/traveler/{username}/excursion/whithout-activity/{activityid}", name="excursion_whithoutactivity")
     * @Method("GET")
     */
    public function getExcursionsWithoutActivityAction($activityid)
    {
        $securityContext = $this->get('security.context');
        $traveler = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException;
        }

        $em = $this->getDoctrine()->getEntityManager();
        $excursions = $em->getRepository("TraventyBundle:Excursion")->findExcursionsWithoutActivity($traveler->getId(), $activityid);

        $result = array();
        foreach ($excursions as $excursion) {
            $result[$excursion->getId()] = $excursion->getName();
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/traveler/{username}/excursion/whithout-resource/{resourceid}", name="excursion_whithoutresource")
     * @Method("GET")
     */
    public function getExcursionsWithoutResourceAction($resourceid)
    {
        $securityContext = $this->get('security.context');
        $traveler = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException;
        }

        $em = $this->getDoctrine()->getEntityManager();
        $excursions = $em->getRepository("TraventyBundle:Excursion")->findExcursionsWithoutResource($traveler->getId(), $resourceid);

        $result = array();
        foreach ($excursions as $excursion) {
            $result[$excursion->getId()] = $excursion->getName();
        }

        $response = new Response(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * @Route("/{excursionid}/activity/add/{activityid}", name="excursion_add_activity", options={"expose" = true})
     * @Method("GET")
     */
    public function addActivityExcursionAction($excursionid, $activityid)
    {
        $securityContext = $this->get('security.context');
        $traveler = $securityContext->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($activityid);
        $excursion = $em->getRepository('TraventyBundle:Excursion')->find($excursionid);

        if ($excursion == null) {
            $this->createNotFoundException("Not excursion found");
        }

        if ($activity == null) {
            $this->createNotFoundException("Not activity found");
        }

        if (false === $securityContext->isGranted('OWNER', $excursion)) {
            throw new AccessDeniedException();
        }

        $excursion->addActivity($activity);
        $em->persist($excursion);
        $em->flush();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $result = array("success" => true, "message" => "Activity added to " . $excursion->getName());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return $this->redirect($this->getRequest()->headers->get('referer'));
        }
    }

    /**
     * @Route("/{excursionid}/resource/add/{resourceid}", name="excursion_add_resource", options={"expose" = true})
     * @Method("GET")
     */
    public function addResourceExcursionAction($excursionid, $resourceid)
    {
        $securityContext = $this->get('security.context');
        $traveler = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($resourceid);
        $excursion = $em->getRepository('TraventyBundle:Excursion')->find($excursionid);

        if ($excursion == null) {
            $this->createNotFoundException("Not excursion found");
        }

        if ($resource == null) {
            $this->createNotFoundException("Not resource found");
        }

        if (false === $securityContext->isGranted('OWNER', $excursion)) {
            throw new AccessDeniedException();
        }

        $excursion->addResource($resource);
        $em->persist($excursion);
        $em->flush();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $result = array("success" => true, "message" => "Resource added to " . $excursion->getName());
            $response = new Response(json_encode($result));
            $response->headers->set('Content-Type', 'application/json');
            return $response;
        } else {
            return $this->redirect($this->getRequest()->headers->get('referer'));
        }
    }

    /**
     * @Route("/{excursionid}/activity/remove/{activityid}", name="excursion_remove_activity")
     * @Method("GET")
     */
    public function removeActivityExcursionAction($excursionid, $activityid)
    {
        $securityContext = $this->get('security.context');

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($activityid);
        $excursion = $em->getRepository('TraventyBundle:Excursion')->find($excursionid);

        if ($excursion == null) {
            $this->createNotFoundException("Not excursion found");
        }

        if ($activity == null) {
            $this->createNotFoundException("Not activity found");
        }

        if (false === $securityContext->isGranted('OWNER', $excursion)) {
            throw new AccessDeniedException();
        }

        $excursion->getActivities()->removeElement($activity);
        $em->persist($excursion);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{excursionid}/resource/remove/{resourceid}", name="excursion_remove_resource")
     * @Method("GET")
     */
    public function removeResourceExcursionAction($excursionid, $resourceid)
    {
        $securityContext = $this->get('security.context');

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($resourceid);
        $excursion = $em->getRepository('TraventyBundle:Excursion')->find($excursionid);

        if ($excursion == null) {
            $this->createNotFoundException("Not excursion found");
        }

        if ($resource == null) {
            $this->createNotFoundException("Not resource found");
        }

        if (false === $securityContext->isGranted('OWNER', $excursion)) {
            throw new AccessDeniedException();
        }

        $excursion->getResources()->removeElement($resource);
        $em->persist($excursion);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

}
