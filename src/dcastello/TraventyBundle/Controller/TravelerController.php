<?php

namespace dcastello\TraventyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use dcastello\TraventyBundle\Entity\Traveler;
use dcastello\TraventyBundle\Entity\Excursion;
use dcastello\TraventyBundle\Form\TravelerBasicInfoType;
use dcastello\TraventyBundle\Form\TravelerAccountInfoType;
use dcastello\TraventyBundle\Form\TravelerSocialNetworkLinkType;
use dcastello\TraventyBundle\Form\EventType;

/**
 * Traveler controller
 *
 * @Route("/traveler")
 */
class TravelerController extends Controller
{

    /**
     * @Route("/{username}/excursion", name="traveler_excursions")
     * @Template("TraventyBundle:Traveler:excursions.html.twig")
     */
    public function getExcursionsAction()
    {
        $traveler = $this->get('security.context')->getToken()->getUser();

        $this->getRequest()->getSession()->set("menuBackend", "excursion");
        return array(
          'excursions' => $traveler->getExcursions()
        );
    }
    
    /**
     * @Route("/{username}/travel", name="traveler_travels")
     * @Template("TraventyBundle:Traveler:travels.html.twig")
     */
    public function getTravelsAction()
    {
        $traveler = $this->get('security.context')->getToken()->getUser();

        $this->getRequest()->getSession()->set("menuBackend", "travel");

        return array(
          'subscriptions' => $traveler->getSubscriptions()
        );
    }    

    /**
     * @Route("/{username}/excursion/{id}/detail", name="traveler_detail_excursion")
     * @Template("TraventyBundle:Excursion:detailExcursion.html.twig")
     */
    public function detailExcursionAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $excursion = $em->getRepository("TraventyBundle:Excursion")->find($id);

        return array(
          'excursion' => $excursion
        );
    }    
    
    /**
     * @Route("/{username}/excursion/{id}/show", name="traveler_show_excursion")
     * @Template("TraventyBundle:Excursion:showExcursion.html.twig")
     */
    public function showExcursionAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $excursion = $em->getRepository("TraventyBundle:Excursion")->find($id);

        if (false === $this->get('security.context')->isGranted('VIEW', $excursion)) {
            throw new AccessDeniedException;
        }

        return array(
          'excursion' => $excursion
        );
    }

    /**
     * @Route("/{username}/excursion/new", name="traveler_new_excursion")
     * @Template("TraventyBundle:Excursion:newExcursion.html.twig")
     */
    public function newExcursionAction()
    {
        $excursionForm = $this->createForm(new EventType(), new Excursion());

        if (false === $this->get('security.context')->isGranted('CREATE')) {
            throw new AccessDeniedException;
        }

        $this->getRequest()->getSession()->set("menuBackend", "excursion");
        return array(
          'update' => false,
          'form' => $excursionForm->createView()
        );
    }

    /**
     * @Route("/{username}/excursion/create", name="traveler_create_excursion")
     * @Method("POST")
     * @Template("TraventyBundle:Excursion:newExcursion.html.twig")
     */
    public function createExcursionAction()
    {
        $traveler = $this->get("security.context")->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $excursion = new Excursion();
        $excursionForm = $this->createForm(new EventType(), $excursion);

        if (false === $this->get('security.context')->isGranted('CREATE')) {
            throw new AccessDeniedException;
        }

        $excursionForm->bindRequest($this->getRequest());

        if ($excursionForm->isValid()) {
            $traveler->addExcursion($excursion);
            $em->persist($traveler);
            $em->flush();

            $this->insertOrUpdateAcl($excursion, $traveler, MaskBuilder::MASK_OWNER);

            $alert = array("title" => "Well done!", "message" => "New excursion created.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('traveler_excursions', array('username' => $traveler->getUsername())));
        }

        return array(
          'update' => false,
          'form' => $excursionForm->createView()
        );
    }

    /**
     * @Route("/{username}/excursion/{id}/delete",name="traveler_delete_excursion")
     * @Template("TraventyBundle:Traveler:excursions.html.twig")
     */
    public function deleteExcursionAction($username, $id)
    {
        $traveler = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $excursion = $em->getRepository("TraventyBundle:Excursion")->find($id);

        if (false === $this->get('security.context')->isGranted('DELETE', $excursion)) {
            throw new AccessDeniedException;
        }

        $traveler->removeExcursion($excursion);
        $em->persist($traveler);
        $em->flush();

        $this->deleteAcl($excursion);

        $alert = array("title" => "Well done!", "message" => "Excursion deleted.");
        $this->getRequest()->getSession()->setFlash("success", $alert);

        return $this->redirect($this->generateUrl('traveler_excursions', array('username' => $traveler->getUsername())));
    }

    /**
     * @Route("/{username}/excursion/{id}/edit", name="traveler_edit_excursion")
     * @Template("TraventyBundle:Excursion:newExcursion.html.twig")
     */
    public function editExcursionAction($username, $id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $excursion = $em->getRepository("TraventyBundle:Excursion")->find($id);

        if (false === $this->get('security.context')->isGranted('EDIT', $excursion)) {
            throw new AccessDeniedException;
        }

        $form = $this->createForm(new EventType(), $excursion);

        return array(
          'id' => $id,
          'update' => true,
          'form' => $form->createView()
        );
    }

    /**
     * @Route("/{username}/excursion/{id}/update", name="traveler_update_excursion")
     * @Method("POST")
     * @Template("TraventyBundle:Excursion:newExcursion.html.twig")
     */
    public function updateExcursionAction($username, $id)
    {
        $traveler = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();
        $excursion = $em->getRepository("TraventyBundle:Excursion")->find($id);

        if (false === $this->get('security.context')->isGranted('EDIT', $excursion)) {
            throw new AccessDeniedException;
        }

        $excursionForm = $this->createForm(new EventType(), $excursion);
        $excursionForm->bindRequest($this->getRequest());

        if ($excursionForm->isValid()) {
            $em->persist($excursion);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Excursion updated.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('traveler_excursions', array('username' => $traveler->getUsername())));
        }

        return array(
          'id' => $id,
          'form' => $excursionForm->createView(),
          'update' => true
        );
    }

    /**
     * @Route("/{username}/favorite-activity", name="traveler_favorites_activities")
     * @Template("TraventyBundle:Traveler:favoritesActivities.html.twig")
     */
    public function getFavoritesActivitiesAction()
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException;
        }

        $this->getRequest()->getSession()->set("menuBackend", "activity");

        return array(
          'title' => 'Favorites Activities',
          'entities' => $user->getFavoritesActivities()
        );
    }

    /**
     * @Route("/{username}/favorite-resource", name="traveler_favorites_resources")
     * @Template("TraventyBundle:Traveler:favoritesResources.html.twig")
     */
    public function getFavoritesResourcesAction()
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException;
        }

        $this->getRequest()->getSession()->set("menuBackend", "resource");

        return array(
          'title' => 'Favorites Activities',
          'entities' => $user->getFavoritesResources()
        );
    }

    /**
     * @Route("/{username}/favorite-activity/{id}", name="traveler_add_favorite_activity")
     */
    public function addFavoriteActivityAction($username, $id)
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($id);

        $user->addFavoriteActivity($activity);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{username}/favorite-activity/remove/{id}", name="traveler_remove_favorite_activity")
     */
    public function removeFavoriteActivityAction($username, $id)
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $activity = $em->getRepository('TraventyBundle:Activity')->find($id);

        $user->getFavoritesActivities()->removeElement($activity);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{username}/favorite-resource/{id}", name="traveler_add_favorite_resource")
     */
    public function addFavoriteResourceAction($username, $id)
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($id);

        $user->addFavoriteResource($resource);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{username}/favorite-resource/remove/{id}", name="traveler_remove_favorite_resource")
     */
    public function removeFavoriteResourceAction($username, $id)
    {
        $securityContext = $this->get('security.context');
        $user = $securityContext->getToken()->getUser();

        if (false === $securityContext->isGranted('ROLE_TRAVELER')) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();

        $resource = $em->getRepository('TraventyBundle:Resource')->find($id);

        $user->getFavoritesResources()->removeElement($resource);
        $em->persist($user);
        $em->flush();

        return $this->redirect($this->getRequest()->headers->get('referer'));
    }

    /**
     * @Route("/{username}", name="traveler_home")
     * @Template()
     */
    public function indexAction($username)
    {
        $this->getRequest()->getSession()->remove("menuBackend");
        return array(
          'title' => 'Traveler settings',
        );
    }

    /**
     * @Route("/{username}/edit", name="traveler_edit")
     * @Template()
     */
    public function editAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $editForm = $this->createForm(new TravelerBasicInfoType(), $user);

        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }

        $this->getRequest()->getSession()->set("menuBackend", "userInfo");

        return array(
          'ruta' => 'traveler_update',
          'title' => 'Basic info',
          'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edits an existing Traveler entity
     * 
     * @Route("/{username}/update", name="traveler_update")
     * @Method("POST")
     * @Template("TraventyBundle:Traveler:edit.html.twig")
     */
    public function updateAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }

        $editForm = $this->createForm(new TravelerBasicInfoType(), $user);
        $editForm->bindRequest($this->getRequest());

        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Data updated correctly.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('traveler_edit', array('username' => $user->getUsername())));
        }

        return array(
          'ruta' => 'traveler_update',
          'title' => 'Basic info',
          'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edit account info
     * 
     * @Route("/{username}/account/edit", name="traveler_account_edit")
     * @Template("TraventyBundle:Traveler:edit.html.twig") 
     */
    public function editPasswordAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }

        $editForm = $this->createForm(new TravelerAccountInfoType(), $user);
        $this->getRequest()->getSession()->set("menuBackend", "changePasswd");

        return array(
          'ruta' => 'traveler_account_update',
          'title' => 'Account info',
          'edit_form' => $editForm->createView()
        );
    }

    /**
     * Update account info
     *
     * @Route("/{username}/account/update", name="traveler_account_update") 
     * @Method("POST")
     * @Template("TraventyBundle:Traveler:edit.html.twig") 
     */
    public function updatePasswordAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }

        $em = $this->getDoctrine()->getEntityManager();
        $editForm = $this->createForm(new TravelerAccountInfoType(), $user);

        $originalPassword = $editForm->getData()->getPassword();
        $editForm->bindRequest($this->getRequest());

        if ($editForm->isValid()) {
            if (null === $user->getPassword()) {
                $user->setPassword($originalPassword);
            } else {
                $encoder = $this->get('security.encoder_factory')->getEncoder($user);
                $codedPassword = $encoder->encodePassword(
                    $user->getPassword(), $user->getSalt()
                );
                $user->setPassword($codedPassword);
            }

            $em->persist($user);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Data updated correctly.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('traveler_account_edit', array('username' => $user->getUsername())));
        }

        return array(
          'ruta' => 'traveler_account_update',
          'title' => 'Account info',
          'edit_form' => $editForm->createView()
        );
    }

    /**
     * Edit social networks links
     * 
     * @Route("/{username}/network/edit", name="traveler_network_edit")
     * @Template("TraventyBundle:Traveler:edit.html.twig")
     */
    public function editSocialNetworkAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }

        $editForm = $this->createForm(new TravelerSocialNetworkLinkType(), $user);
        $this->getRequest()->getSession()->set("menuBackend", "socialNetworkEdit");

        return array(
          'ruta' => 'traveler_network_update',
          'title' => 'Social network links',
          'edit_form' => $editForm->createView()
        );
    }

    /**
     * Update social network links
     * 
     * @Route("/{username}/network/update", name="traveler_network_update")
     * @Method("POST") 
     * @Template("TraventyBundle:Traveler:edit.html.twig")
     */
    public function updateSocialNetworkAction($username)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        
        if (false === $this->get('security.context')->isGranted('OWNER', $user)) {
            throw new AccessDeniedException();
        }
        
        $em = $this->getDoctrine()->getEntityManager();
        $editForm = $this->createForm(new TravelerSocialNetworkLinkType(), $user);

        $editForm->bindRequest($this->getRequest());
        if ($editForm->isValid()) {
            $em->persist($user);
            $em->flush();

            $alert = array("title" => "Well done!", "message" => "Data updated correctly.");
            $this->getRequest()->getSession()->setFlash("success", $alert);

            return $this->redirect($this->generateUrl('traveler_network_edit', array('username' => $user->getUsername())));
        }

        return array(
          'ruta' => 'traveler_network_update',
          'title' => 'Social network links',
          'edit_form' => $editForm->createView()
        );
    }

    private function insertOrUpdateAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->container->get('security.acl.provider');

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

    private function deleteAcl($object)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $this->get('security.acl.provider')->deleteAcl($idObject);
    }

}
