<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class TravelerType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('country')
            ->add('city')
            ->add('state')
            ->add('telephone')
            ->add('twitter')
            ->add('web')
            ->add('email')
            ->add('password')
            ->add('surname')
            ->add('birthday')
            ->add('nif')
        ;
    }

    public function getName()
    {
        return 'dcastello_traventybundle_travelertype';
    }
    
    public function getDefaultOptions(array $options)
    {
        $options['data_class'] = 'dcastello\TraventyBundle\Entity\Traveler';
        return $options;
    }
}
