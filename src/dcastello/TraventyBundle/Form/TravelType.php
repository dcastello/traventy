<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class TravelType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('active')
            ->add('date','date', array(
                'widget' => 'choice',
                'years'  => range(date("Y"),date("Y") + 6)
            ))
            ->add('price', 'money')
            ->add('attendantLimit', 'integer', array(
                'label' => 'Attendant limit'
            ))
        ;
    }

    public function getName()
    {
        return 'agencyTravel';
    }
}
