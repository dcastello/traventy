<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Doctrine\ORM\EntityRepository;

class ActivitySearchType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name', 'text', array('required' => false))
            ->add('date', 'date', array(
                'required' => false,
                'input'    => 'datetime',
                'widget'   => 'single_text',
                'format'   => 'dd-MM-yyyy',
            ))
            ->add('city', null, array(
              'required'     => false,
              'empty_value'  => '',
              'empty_data'   => null,
              'query_builder' => function(EntityRepository $er) {
                  return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
              }
            ))
        ;
    }

    public function getName()
    {
        return 'activitysearchtype';
    }

    public function getDefaultOptions(array $options)
    {
        $options['data_class'] = 'dcastello\TraventyBundle\Entity\Activity';
        return $options;
    }

}
