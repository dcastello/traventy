<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class TravelerBasicInfoType extends AbstractType
{
   public function buildForm(FormBuilder $builder, array $options)
   {
      $builder
          ->add('name')
          ->add('surname')
          ->add('birthday', 'birthday')
          ->add('nif')
          ->add('telephone');
   }

   public function getName()
   {
      return 'travelerBasicInfo';
   }

   public function getDefaultOptions(array $options)
   {
      $options['data_class'] = 'dcastello\TraventyBundle\Entity\Traveler';
      $options['validation_groups'] = array('basicInfo');
      return $options;
   }
}
