<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EventType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('active', 'checkbox', array(
                'label'    => "Active"))
        ;
    }

    public function getName()
    {
        return 'travelerEvent';
    }
}
