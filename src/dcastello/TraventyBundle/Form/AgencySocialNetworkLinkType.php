<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AgencySocialNetworkLinkType extends AbstractType
{

   public function buildForm(FormBuilder $builder, array $options)
   {
      $builder
          ->add('web')
          ->add('twitter');
   }

   public function getName()
   {
      return 'agencyNetworkLink';
   }

   public function getDefaultOptions(array $options)
   {
      $options['data_class'] = 'dcastello\TraventyBundle\Entity\Agency';
      $options['validation_groups'] = array('socialNetworkLink');
      return $options;
   }

}

