<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ImageActivityType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('image', 'file', array('required' => false))
        ;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'dcastello\TraventyBundle\Entity\ImageActivity',
        );
    }

    public function getName()
    {
        return 'traventy_activityimage_form';
    }

}
