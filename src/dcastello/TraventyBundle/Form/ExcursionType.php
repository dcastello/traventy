<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class ExcursionType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('active')
            ->add('activities')
            ->add('resources')
        ;
    }

    public function getName()
    {
        return 'travelerExcursion';
    }
}
