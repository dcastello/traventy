<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class TravelerAccountInfoType extends AbstractType
{

   public function buildForm(FormBuilder $builder, array $options)
   {
      $builder
          ->add('username')
          ->add('email', 'email')
          ->add('password', 'repeated', array(
                'type'            => 'password',
                'invalid_message' => 'The password dont\' match',
                'options' => array('label' => 'Password'),
                'required' => false
            ))
      ;
   }

   public function getName()
   {
      return 'travelerAccountInfo';
   }

   public function getDefaultOptions(array $options)
   {
      $options['data_class'] = 'dcastello\TraventyBundle\Entity\Traveler';
      $options['validation_groups'] = array('accountInfo');
      return $options;
   }

}

