<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Doctrine\ORM\EntityRepository;

class ResourceType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('name')
                ->add('city', null, array(
                    'required' => true,
                    'empty_value' => false,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                    }
                ))
                ->add('telephone')
                ->add('fax')
                ->add('web')
        ;
    }

    public function getName()
    {
        return 'dcastello_traventybundle_resourcetype';
    }

}
