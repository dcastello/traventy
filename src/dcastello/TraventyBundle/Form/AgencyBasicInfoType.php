<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AgencyBasicInfoType extends AbstractType
{
   public function buildForm(FormBuilder $builder, array $options)
   {
      $builder
          ->add('name')
          ->add('cif')
          ->add('telephone')
          ->add('fax')
          ->add('description');
   }

   public function getName()
   {
      return 'agencyBasicInfo';
   }

   public function getDefaultOptions(array $options)
   {
      $options['data_class'] = 'dcastello\TraventyBundle\Entity\Agency';
      $options['validation_groups'] = array('basicInfo');
      return $options;
   }
}
