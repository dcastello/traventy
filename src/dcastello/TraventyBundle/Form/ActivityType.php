<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Doctrine\ORM\EntityRepository;

class ActivityType extends AbstractType
{

    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
                ->add('name', 'text', array('required' => true))
                ->add('city', null, array(
                    'required' => true,
                    'empty_value' => false,
                    'query_builder' => function(EntityRepository $er) {
                        return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
                    }
                ))
                ->add('date', 'date', array(
                    'required' => false,
                    'input' => 'datetime',
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy',
                    'invalid_message' => 'You are entered an invalid message. Format: day-month-year'
                ))
                ->add('description')
                ->add('images', 'collection', array(
                    'required' => false,
                    'type' => new ImageActivityType(),
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false
                ))
        ;
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'dcastello\TraventyBundle\Entity\Activity',
        );
    }

    public function getName()
    {
        return 'traventy_activity_form';
    }

}
