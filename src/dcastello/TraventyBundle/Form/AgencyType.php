<?php

namespace dcastello\TraventyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class AgencyType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('address')
            ->add('country')
            ->add('city')
            ->add('state')
            ->add('telephone')
            ->add('twitter')
            ->add('web')
            ->add('email')
            ->add('password')
            ->add('cif')
            ->add('fax')
            ->add('description')
        ;
    }

    public function getName()
    {
        return 'dcastello_traventybundle_agencytype';
    }
}
