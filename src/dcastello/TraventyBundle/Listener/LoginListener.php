<?php

namespace dcastello\TraventyBundle\Listener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Router;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LoginListener
{
    private $router;
    private $context;
    private $userType;
    private $username;

    const TRAVELER_USER = 'TRAVELER';
    const AGENCY_USER = 'AGENCY';

    public function __construct(Router $router, SecurityContext $context)
    {
        $this->router = $router;
        $this->context = $context;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $token = $event->getAuthenticationToken();
        $this->userType = $token->getUser()->getType();
        $this->username = $token->getUser()->getUsername();
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $portada = null;
        
        if (strtoupper($this->userType) == self::TRAVELER_USER) {
            $portada = $this->router->generate("traveler_home", array('username' => $this->username));
        } elseif (strtoupper($this->userType) == self::AGENCY_USER) {
            $portada = $this->router->generate("agency_home", array('username' => $this->username));
        }

        if (isset($portada)) {
            $event->setResponse(new RedirectResponse($portada));
            $event->stopPropagation();
        }
    }

}