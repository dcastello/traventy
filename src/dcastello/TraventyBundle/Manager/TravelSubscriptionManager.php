<?php

namespace dcastello\TraventyBundle\Manager;

use Doctrine\ORM\EntityManager;
use dcastello\TraventyBundle\Entity\Travel;
use dcastello\TraventyBundle\Entity\Traveler;
use dcastello\TraventyBundle\Entity\TravelSubscription;
use dcastello\TraventyBundle\Entity\Exception\AttendantLimitExceededException;
use dcastello\TraventyBundle\Entity\Exception\DuplicatedSubscriptionException;
use dcastello\TraventyBundle\Entity\Exception\SubscriptionNotExistException;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use Symfony\Component\Security\Acl\Dbal\MutableAclProvider;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class TravelSubscriptionManager
{
    private $entityManager;
    private $aclProvider;
    private $class;
    private $repository;

    function __construct(EntityManager $entityManager, MutableAclProvider $aclProvider, $class)
    {
        $this->entityManager = $entityManager;
        $this->aclProvider = $aclProvider;
        $this->class = $class;
        $this->repository = $entityManager->getRepository($class);
    }

    public function getAclProvider()
    {
        return $this->aclProvider;
    }

    public function getEntityManager()
    {
        return $this->entityManager;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function createSubscription()
    {
        $class = $this->class;
        $travelSubscription = new $class;

        return $travelSubscription;
    }

    public function addSubscription(Travel $travel, Traveler $traveler)
    {
        if ($travel->getSubscriptions()->count() >= $travel->getAttendantLimit()) {
            throw new AttendantLimitExceededException;
        }

        if ($this->getRepository()->existSubscription($travel, $traveler)) {
            throw new DuplicatedSubscriptionException;
        }

        $subscription = $this->createSubscription();
        $subscription->setTravel($travel);
        $subscription->setTraveler($traveler);
        $this->getEntityManager()->persist($subscription);
        $this->getEntityManager()->flush();
        
        $this->insertOrUpdateAcl($subscription, $travel->getAgency(), MaskBuilder::MASK_OWNER);
    }

    public function acceptSubscription(Travel $travel, Traveler $traveler)
    {
        $subscription = $this->getRepository()->findOneBy(array('travel' => $travel->getId(), 'traveler' => $traveler->getId()));

        if (!$subscription) {
            throw new SubscriptionNotExistException;
        }

        $subscription->setStatus(TravelSubscription::STATUS_ACCEPTED);
        $this->getEntityManager()->persist($subscription);
        $this->getEntityManager()->flush();
    }

    public function denySubscription(Travel $travel, Traveler $traveler)
    {
        $subscription = $this->getRepository()->findOneBy(array('travel' => $travel->getId(), 'traveler' => $traveler->getId()));

        if (!$subscription) {
            throw new SubscriptionNotExistException;
        }

        $subscription->setStatus(TravelSubscription::STATUS_DENIED);
        $this->getEntityManager()->persist($subscription);
        $this->getEntityManager()->flush();
    }

    private function insertOrUpdateAcl($object, $user, $permision)
    {
        $idObject = ObjectIdentity::fromDomainObject($object);
        $idUser = UserSecurityIdentity::fromAccount($user);

        $provider = $this->getAclProvider();

        try {
            $acl = $provider->findAcl($idObject, array($idUser));
        } catch (AclNotFoundException $exception) {
            $acl = $provider->createAcl($idObject);
        }

        $aces = $acl->getObjectAces();
        foreach ($aces as $index => $ace) {
            $acl->deleteObjectAce($ace);
        }

        $acl->insertObjectAce($idUser, $permision);
        $provider->updateAcl($acl);
    }

}
