var collectionHolder = $('ul.images-activity');
var $addImageLink = $('<a class="btn btn-mini btn-primary" href="#">Add image</a>');
var $newLinkLi = $('<li class="span3"></li>').append($addImageLink);

$(document).ready(function() {
    collectionHolder.append($newLinkLi);
    collectionHolder.find('div.caption').each(function() {
        addImageFormDeleteLink($(this));
    });
    
    $addImageLink.on('click', function(e) {
        e.preventDefault();
        
        addImageForm(collectionHolder, $newLinkLi)
    });
});

function addImageForm(collectionHolder, $newLinkLi) {
    
    var prototype = collectionHolder.attr('data-prototype');
    
    var newForm = prototype.replace(/\$\$name\$\$/g, collectionHolder.children().length);
    
    var $newFormLi = $('<li class="span3"></li>').append(newForm);
    $newLinkLi.before($newFormLi);
    
    addImageFormDeleteLink($newFormLi);
}

function addImageFormDeleteLink($imageFormLi) {
    var $removeFormA = $('<a class="btn btn-mini btn-danger" href="#">Delete image</a>');
    $imageFormLi.append($removeFormA);

    $removeFormA.on('click', function(e) {
        e.preventDefault();

        $imageFormLi.remove();
    });    
}