<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use dcastello\TraventyBundle\Entity\Activity;
use dcastello\TraventyBundle\Entity\Resource;

/**
 * dcastello\TraventyBundle\Entity\Event
 * 
 * @ORM\Table(name="event")
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discriminator", type="string")
 * @ORM\DiscriminatorMap({"agency" = "Travel", "traveler" = "Excursion"})
 */
class Event
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=4,
     *     message="The name must have at least {{ limit }} characters"
     * )
     */
    private $name;
    
    /**
     * @var boolean $active
     * 
     * @ORM\Column(name="active", type="boolean")
     */
    private $active = true;

    /**
     * @ORM\ManyToMany(targetEntity="Activity")
     * @ORM\JoinTable(name="event_activity",
     *      joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="activity_id", referencedColumnName="id")}
     *      )
     */
    private $activities;
    
    /**
     * @ORM\ManyToMany(targetEntity="Resource")
     * @ORM\JoinTable(name="event_resource",
     *      joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="resource_id", referencedColumnName="id")}
     *      )
     */
    private $resources;
    
    public function __construct() {
        $this->activities = new \Doctrine\Common\Collections\ArrayCollection();
        $this->resources = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Set active
     * 
     * @param boolean $active 
     */
    public function setActive($active)
    {
       $this->active = $active;
    }
    
    /**
     * Get active
     * 
     * @return boolean
     */
    public function getActive()
    {
       return $this->active;
    }
    
    /**
     * Get Activities
     * 
     * @return Doctrine\Common\Collections\Collection $activities
     */
    public function getActivities()
    {
       return $this->activities;
    }
    
    /*
     * Add Activity
     * 
     * @param dcastello\TraventyBundle\Entity\Activity $activity
     */
    public function addActivity(Activity $activity) 
    {
        if (!$this->activities->contains($activity))
        {
            $this->activities->add($activity);
        }
    }
    
    /**
     * Get Resources
     * 
     * @return Doctrine\Common\Collections\Collection $resources
     */
    public function getResources()
    {
       return $this->resources;
    }
    
    /*
     * Add Resource
     * 
     * @param dcastello\TraventyBundle\Entity\Resource $resource
     */
    public function addResource(Resource $resource) 
    {
        if (!$this->resources->contains($resource))
        {
            $this->resources->add($resource);
        }
    }    
}