<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * dcastello\TraventyBundle\Entity\TravelSubscription
 *
 * @ORM\Table(
 *     name="travel_traveler",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="travel_traveler_idx", columns={"travel_id", "traveler_id"})}
 * )
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\TravelSubscriptionRepository")
 * @UniqueEntity({"travel","traveler"})
 */
class TravelSubscription
{
    const STATUS_PENDING = 'Pending';
    const STATUS_ACCEPTED = 'Accepted';
    const STATUS_DENIED = 'Denied';

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="Travel", inversedBy="subscriptions")
     */
    private $travel;
    /**
     * @ORM\ManyToOne(targetEntity="Traveler", inversedBy="subscriptions")
     */
    private $traveler;
    /**
     * @ORM\Column(name="status", type="string", length="100")
     */
    private $status;

    public function __construct()
    {
        $this->status = self::STATUS_PENDING;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTravel()
    {
        return $this->travel;
    }

    public function setTravel($travel)
    {
        $this->travel = $travel;
    }

    public function getTraveler()
    {
        return $this->traveler;
    }

    public function setTraveler($traveler)
    {
        $this->traveler = $traveler;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        if (!in_array($status, array(self::STATUS_PENDING, self::STATUS_ACCEPTED, self::STATUS_DENIED))) {
            throw new \InvalidArgumentException("Invalid status");
        }

        $this->status = $status;
    }

}
