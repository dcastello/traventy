<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * dcastello\TraventyBundle\Entity\Resource
 *
 * @ORM\Table(name="resource")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\ResourceRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Resource
{
    const DEFAULT_COUNTRY = 'Spain';

    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\ManyToMany(targetEntity="Traveler", mappedBy="favoritesResources")
     */
    private $favoritesTravelers;
    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=5,
     *     message="The minimum characters are {{ limit }}"
     * )      
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;
    /**
     * @var string $country
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;
    /**
     * @var string $telephone
     *
     * @ORM\Column(name="telephone", type="string", length=12)
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=9,
     *     message="The minimum characters are {{ limit }}"
     * )
     */
    private $telephone;
    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=12)
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=9,
     *     message="The minimum characters are {{ limit }}"
     * )
     */
    private $fax;
    /**
     * @var string $web
     *
     * @ORM\Column(name="web", type="string", length=255)
     * @Assert\Url
     */
    private $web;

    public function __construct()
    {
        $this->favoritesTravelers = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getFavoritesTravelers()
    {
        return $this->favoritesTravelers;
    }

    public function addFavoriteTraveler(Traveler $traveler)
    {
        if (!$this->favoritesTravelers->contains($traveler)) {
            $this->favoritesTravelers[] = $traveler;
        }
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Get fax
     *
     * @return string 
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set web
     *
     * @param string $web
     */
    public function setWeb($web)
    {
        $this->web = $web;
    }

    /**
     * Get web
     *
     * @return string 
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaultValues()
    {
        $this->setCountry(self::DEFAULT_COUNTRY);
    }

}