<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ActivityRepository extends EntityRepository
{

    public function findTop($numberTopActivities = 1)
    {
        return $this->findBy(array(), array('name' => 'ASC'), $numberTopActivities);
    }

    public function queryAllActivities()
    {
        $query = $this->createQueryBuilder('a')
                ->orderBy('a.name', 'ASC')
                ->getQuery();
        
        return $query;
    }
    
    public function findAllActivities()
    {
        return $this->queryAllActivities()->getResult();
    }    

    public function numberTotalActivities()
    {
        $totalActivities = $this->createQueryBuilder('a')
                ->select("COUNT(a.id)")
                ->getQuery()
                ->getSingleScalarResult();

        return $totalActivities;
    }

}