<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use dcastello\TraventyBundle\Util\Util;

/**
 * dcastello\TraventyBundle\Entity\State
 *
 * @ORM\Table(name="state")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\StateRepository")
 * @UniqueEntity("slug")
 * @ORM\HasLifecycleCallbacks()
 */
class State
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $slug;
    /**
     * @ORM\OneToMany(targetEntity="City", mappedBy="city")
     */
    private $cities;

    public function __construct()
    {
        $this->cities = new ArrayCollection;
    }
    
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $slug 
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return ArrayCollection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @param City $city
     */
    public function addCity(City $city)
    {
        if (!$this->getCities()->contains($city)) {
            $this->cities->add($city);
            $city->setState($this);
        }
    }
    
    /**
     * @ORM\PrePersist 
     */
    public function setSlugValue()
    {
        $this->setSlug(Util::Slugify($this->getName()));
    }

}