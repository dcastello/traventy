<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use dcastello\TraventyBundle\Util\Util;

/**
 * dcastello\TraventyBundle\Entity\City
 *
 * @ORM\Table(
 *      name="city",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="longitude_latitude_idx", columns={"latitude", "longitude"})}
 * )
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\CityRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("slug")
 * @UniqueEntity({"latitude","longitude"})
 */
class City
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $slug;
    /**
     * @ORM\Column(type="decimal", precision=17, scale=7, nullable=false)
     */
    private $latitude;
    /**
     * @ORM\Column(type="decimal", precision=17, scale=7)
     */
    private $longitude;
    /**
     * @ORM\Column(name="postal_code", type="string", length=10)
     */
    private $postalCode;
    /**
     * @ORM\ManyToOne(targetEntity="State", inversedBy="cities" )
     */
    private $state;
    
    public function __toString()
    {
        return $this->getName();
    }

    public function setId($id)
    {
        $this->id = $id;
    }    
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * 
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     * 
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $slug 
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param decimal $latitude 
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return decimal
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param decimal $longitude 
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return decimal $longitude
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param string $postalCode 
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }
    
    /**
     * @param State $state 
     */
    public function setState(State $state)
    {
        $this->state = $state;
    }
    
    /**
     * @return State 
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * @ORM\PrePersist 
     */
    public function setSlugValue()
    {
        $this->setSlug(Util::Slugify($this->getName()));
    }

}