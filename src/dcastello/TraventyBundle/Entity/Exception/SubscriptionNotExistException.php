<?php

/**
 * Description of SubscriptionNotExistException
 *
 * @author david
 */

namespace dcastello\TraventyBundle\Entity\Exception;

class SubscriptionNotExistException extends \LogicException
{
    public function __construct($message = "Subscription not exist.", \Exception $previous = null)
    {
        parent::__construct($message, 404, $previous);
    }    
}
