<?php

/**
 * Description of AttendantLimitExcededException
 *
 * @author david
 */

namespace dcastello\TraventyBundle\Entity\Exception;
      
class AttendantLimitExceededException extends \LogicException
{

    public function __construct($message = "Attendant limit exceeded", \Exception $previous = null)
    {
        parent::__construct($message, 202, $previous);
    }

}
