<?php

/**
 * Description of TravelerSubscribedException
 *
 * @author david
 */

namespace dcastello\TraventyBundle\Entity\Exception;

class DuplicatedSubscriptionException extends \LogicException
{

    public function __construct($message = "Duplicated Subscription. You are already subscribed.", \Exception $previous = null)
    {
        parent::__construct($message, 202, $previous);
    }

}
