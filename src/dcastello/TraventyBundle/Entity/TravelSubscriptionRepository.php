<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;
use dcastello\TraventyBundle\Entity\Travel;
use dcastello\TraventyBundle\Entity\Traveler;
use dcastello\TraventyBundle\Entity\TravelSubscription;

class TravelSubscriptionRepository extends EntityRepository
{

    public function existSubscription(Travel $travel, Traveler $traveler)
    {
        $results = $this->findBy(array('travel' => $travel->getId(), 'traveler' => $traveler->getId()));
        return (count($results) > 0) ? true : false;
    }

    public function findTravelersWithAcceptedSubscriptionsByTravel($travelId)
    {
        return $this->findTravelersSubscriptionsFor(TravelSubscription::STATUS_ACCEPTED, $travelId);
    }

    public function findTravelersWithPendingSubscriptionsByTravel($travelId)
    {
        return $this->findTravelersSubscriptionsFor(TravelSubscription::STATUS_PENDING, $travelId);
    }
    
    public function findTravelersWithDeniedSubscriptionsByTravel($travelId)
    {
        return $this->findTravelersSubscriptionsFor(TravelSubscription::STATUS_DENIED, $travelId);
    }    

    public function findTravelersSubscriptionsFor($status, $travelId)
    {

        $query = $this->getEntityManager()->createQuery('
                    SELECT t FROM TraventyBundle:Traveler t
                    JOIN t.subscriptions s
                    JOIN s.travel tl
                    WHERE s.status = :status
                    AND tl.id = :travelId')
                ->setParameter('status', $status)
                ->setParameter('travelId', $travelId);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function findAcceptedSubscriptionsByTravel($travelId)
    {
        return $this->findSubscriptionsFor(TravelSubscription::STATUS_ACCEPTED, $travelId);
    }

    public function findPendingSubscriptionsByTravel($travelId)
    {
        return $this->findSubscriptionsFor(TravelSubscription::STATUS_PENDING, $travelId);
    }
    
    public function findDeniedSubscriptionsByTravel($travelId)
    {
        return $this->findSubscriptionsFor(TravelSubscription::STATUS_DENIED, $travelId);
    }    

    public function findSubscriptionsFor($status, $travelId)
    {
        $query = $this->getEntityManager()->createQuery('
                    SELECT ts FROM TraventyBundle:TravelSubscription ts
                    JOIN ts.travel tl
                    WHERE ts.status = :status
                    AND tl.id = :travelId')
                ->setParameter('status', $status)
                ->setParameter('travelId', $travelId);

        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
