<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TravelRepository extends EntityRepository
{

    public function findTop($numberTopTravels = 1)
    {
        return $this->findBy(array(), array('name' => 'ASC'), $numberTopTravels);
    }

    public function findTravelsWithoutActivity($agencyId, $activityId)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
                ->select('e')
                ->from('TraventyBundle:Travel', 'e')
                ->join('e.activities', 'a')
                ->join('e.agency', 't')
                ->Where('a.id = :activityId')
                ->andWhere('t.id = :agencyId')
                ->setParameter('activityId', $activityId)
                ->setParameter('agencyId', $agencyId)
                ->getQuery();

        $travels = $query->getResult();

        $idTravelList = array();
        foreach ($travels as $travel) {
            $idTravelList[] = $travel->getId();
        }

        if (count($idTravelList) == 0) {
            $query = $this->getEntityManager()->createQueryBuilder()
                    ->select('e')
                    ->from('TraventyBundle:Travel', 'e')
                    ->join('e.agency', 't')
                    ->Where('t.id = :agencyId')
                    ->setParameter('agencyId', $agencyId)
                    ->getQuery();
        } else {
            $query = $this->getEntityManager()->createQueryBuilder()
                    ->select('e')
                    ->from('TraventyBundle:Travel', 'e')
                    ->join('e.agency', 't')
                    ->Where('t.id = :agencyId')
                    ->andWhere('e.id not in (' . implode(",", $idTravelList) . ')')
                    ->setParameter('agencyId', $agencyId)
                    ->getQuery();
        }

        return $query->getResult();
    }

    public function findExcursionsWithoutResource($agencyId, $resourceId)
    {
        $query = $this->getEntityManager()->createQueryBuilder()
                ->select('e')
                ->from('TraventyBundle:Travel', 'e')
                ->join('e.resources', 'a')
                ->join('e.agency', 't')
                ->Where('a.id = :resourceId')
                ->andWhere('t.id = :agencyId')
                ->setParameter('resourceId', $resourceId)
                ->setParameter('agencyId', $agencyId)
                ->getQuery();

        $travels = $query->getResult();

        $idTravelList = array();
        foreach ($travels as $travel) {
            $idTravelList[] = $travel->getId();
        }

        if (count($idTravelList) == 0) {
            $query = $this->getEntityManager()->createQueryBuilder()
                    ->select('e')
                    ->from('TraventyBundle:Travel', 'e')
                    ->join('e.agency', 't')
                    ->Where('t.id = :agencyId')
                    ->setParameter('agencyId', $agencyId)
                    ->getQuery();
        } else {
            $query = $this->getEntityManager()->createQueryBuilder()
                    ->select('e')
                    ->from('TraventyBundle:Travel', 'e')
                    ->join('e.agency', 't')
                    ->Where('t.id = :agencyId')
                    ->andWhere('e.id not in (' . implode(",", $idTravelList) . ')')
                    ->setParameter('agencyId', $agencyId)
                    ->getQuery();
        }

        return $query->getResult();
    }

}
