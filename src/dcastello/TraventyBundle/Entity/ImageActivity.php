<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * dcastello\TraventyBundle\Entity\Image
 *
 * @ORM\Table(name="image_activity")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\ImageActivityRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ImageActivity
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="image", type="string", length=255)
     * @Assert\Image(maxSize="500k")
     */
    private $image;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="Activity", inversedBy="images") 
     */
    private $activity;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getActivity()
    {
        return $this->activity;
    }

    public function setActivity($activity)
    {
        $this->activity = $activity;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getUploadDir()
    {
        return 'uploads/images/activity';
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getWebPath()
    {
        return null == $this->name ? null : $this->getUploadDir() . '/' . $this->name;
    }

    public function getAbsolutePath()
    {
        return null == $this->name ? null : $this->getUploadRootDir() . '/' . $this->name;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
        if (null !== $this->image) {
            $this->name = uniqid("activity-") . '-image.' . $this->image->guessExtension();
        }
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate() 
     */
    public function upload()
    {
        if (null === $this->image) {
            return;
        }

        $this->image->move($this->getUploadRootDir(), $this->name);

        unset($this->image);
    }

    /**
     * @ORM\PostRemove() 
     */
    public function removeUpload()
    {
        if ($photo = $this->getAbsolutePath()) {
            unlink($photo);
        }
    }

}