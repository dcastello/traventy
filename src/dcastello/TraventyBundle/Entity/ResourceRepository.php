<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ResourceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ResourceRepository extends EntityRepository
{

    public function findTop($numberTopResources = 1)
    {
        return $this->findBy(array(), array('name' => 'ASC'), $numberTopResources);
    }

    public function findAllResources()
    {
        return $this->queryAllResources()->getResult();
    }

    public function queryAllResources()
    {
        $query = $this->createQueryBuilder('r')
                ->orderBy('r.name', 'ASC')
                ->getQuery();

        return $query;
    }

    public function numberTotalResources()
    {
        $totalResources = $this->createQueryBuilder('r')
                ->select("COUNT(r.id)")
                ->getQuery()
                ->getSingleScalarResult();

        return $totalResources;
    }

}