<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * dcastello\TraventyBundle\Entity\Agency
 * 
 * @ORM\Table(name="agency")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\AgencyRepository")
 */
class Agency extends User
{    
    /**
     * @var string $cif
     *
     * @ORM\Column(name="cif", type="string", length=15)
     */
    private $cif;
    
    /**
     * @var string $fax
     *
     * @ORM\Column(name="fax", type="string", length=20, nullable=true)
     */
    private $fax;
    
    /**
     * @var string $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @ORM\OneToMany(targetEntity="Travel", mappedBy="agency", cascade={"persist", "remove"})
     */
    private $travels;    

    
    public function __construct() {
        $this->travels = new ArrayCollection();
    }

    /**
     * Set cif
     *
     * @param string $cif
     */
    public function setCif($cif)
    {
        $this->cif = $cif;
    }
    
    /**
     * Get CIF
     * 
     * @return string
     */
    public function getCif()
    {
        return $this->cif;
    }
    
    /**
     * Set fax
     * 
     * @param type $fax 
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }
    
    /**
     * Get Fax
     * 
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }
    
    /**
     * Set description
     * 
     * @param type $description 
     */
    public function setDescrition($description)
    {
        $this->description = $description;
    }
    
    /**
     * Get Description
     * 
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getTravels() {
        return $this->travels;
    }

    public function setTravels($travels) {
        $this->travels = $travels;
    }
    
    public function addTravel(Travel $travel)
    {
        if (!$this->travels->contains($travel)) {
            $this->travels->add($travel);
            $travel->setAgency($this);
        }
    }
    
    public function removeTravel(Travel $travel) {
        if ($this->travels->contains($travel)) {
            $this->travels->removeElement($travel);
            $travel->setAgency(null);
        }
    }
    
    function getRoles()
    {
        return array('ROLE_AGENCY');
    }    
    
    function getType()
    {
        return 'AGENCY';
    }    
}
