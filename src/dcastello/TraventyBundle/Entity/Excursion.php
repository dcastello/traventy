<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * dcastello\TraventyBundle\Entity\TravelerEvent
 * 
 * @ORM\Table(name="excursion")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\ExcursionRepository")
 */
class Excursion extends Event
{
    /**
     * @ORM\ManyToOne(targetEntity="Traveler", inversedBy="excursions")
     * @ORM\JoinColumn(name="traveler_id", referencedColumnName="id")
     */
    private $traveler;

    public function setTraveler(Traveler $traveler = null)
    {
        $this->traveler = $traveler;
    }

    public function getTraveler()
    {
        return $this->traveler;
    }

    public function getType()
    {
        return 'EXCURSION';
    }

}
