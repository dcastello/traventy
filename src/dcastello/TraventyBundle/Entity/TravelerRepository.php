<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;

class TravelerRepository extends EntityRepository
{
   public function findFavoritesActivitiesId($travelerId)
   {
      $query = $this->getEntityManager()->createQueryBuilder()
              ->select('a.id')
              ->from('TraventyBundle:Activity', 'a')
              ->join('a.favoritesTravelers', 'f')
              ->where('f.id = :travelerId')
              ->setParameter('travelerId', $travelerId)
              ->getQuery();
      
      $activitiesIds = $query->getResult();
      
      $result = array();
      foreach ($activitiesIds as $activityId)
      {
         $result[] = $activityId["id"];
      }
      
      return $result;
   }
   
   public function findFavoritesResourcesId($travelerId)
   {
      $query = $this->getEntityManager()->createQueryBuilder()
              ->select('a.id')
              ->from('TraventyBundle:Resource', 'a')
              ->join('a.favoritesTravelers', 'f')
              ->where('f.id = :travelerId')
              ->setParameter('travelerId', $travelerId)
              ->getQuery();
      
      $resourcesIds = $query->getResult();
      
      $result = array();
      foreach ($resourcesIds as $resourceId)
      {
         $result[] = $resourceId["id"];
      }
      
      return $result;
   }   
}
