<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ExcursionRepository extends EntityRepository
{
   public function findTop($numberTopExcursions = 1)
   {
       return $this->findBy(array(), array('name' => 'ASC'), $numberTopExcursions);
   }
   
   public function findExcursionsWithoutActivity($travelerId, $activityId)
   {
      $query = $this->getEntityManager()->createQueryBuilder()
          ->select('e')
          ->from('TraventyBundle:Excursion', 'e')
          ->join('e.activities', 'a')
          ->join('e.traveler', 't')
          ->Where('a.id = :activityId')
          ->andWhere('t.id = :travelerId')
          ->setParameter('activityId', $activityId)
          ->setParameter('travelerId', $travelerId)
          ->getQuery();
      
      $excursions = $query->getResult();
      
      $idExcursionList = array();
      foreach ($excursions as $excursion)
      {
         $idExcursionList[] = $excursion->getId();         
      }
      
      if (count($idExcursionList) == 0)
      {      
         $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('TraventyBundle:Excursion', 'e')
            ->join('e.traveler', 't')
            ->Where('t.id = :travelerId')
            ->setParameter('travelerId', $travelerId)
            ->getQuery();
      }
      else
      {
         $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('TraventyBundle:Excursion', 'e')
            ->join('e.traveler', 't')
            ->Where('t.id = :travelerId')              
            ->andWhere('e.id not in ('.implode(",", $idExcursionList).')')
            ->setParameter('travelerId', $travelerId)
            ->getQuery();         
      }

      return $query->getResult();
   }
   
   public function findExcursionsWithoutResource($travelerId, $resourceId)
   {
      $query = $this->getEntityManager()->createQueryBuilder()
          ->select('e')
          ->from('TraventyBundle:Excursion', 'e')
          ->join('e.resources', 'a')
          ->join('e.traveler', 't')
          ->Where('a.id = :resourceId')
          ->andWhere('t.id = :travelerId')
          ->setParameter('resourceId', $resourceId)
          ->setParameter('travelerId', $travelerId)
          ->getQuery();
      
      $excursions = $query->getResult();
      
      $idExcursionList = array();
      foreach ($excursions as $excursion)
      {
         $idExcursionList[] = $excursion->getId();         
      }
      
      if (count($idExcursionList) == 0)
      {      
         $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('TraventyBundle:Excursion', 'e')
            ->join('e.traveler', 't')
            ->Where('t.id = :travelerId')
            ->setParameter('travelerId', $travelerId)
            ->getQuery();
      }
      else
      {
         $query = $this->getEntityManager()->createQueryBuilder()
            ->select('e')
            ->from('TraventyBundle:Excursion', 'e')
            ->join('e.traveler', 't')
            ->Where('t.id = :travelerId')              
            ->andWhere('e.id not in ('.implode(",", $idExcursionList).')')
            ->setParameter('travelerId', $travelerId)
            ->getQuery();         
      }

      return $query->getResult();
   }   

}
