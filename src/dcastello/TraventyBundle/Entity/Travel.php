<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * dcastello\TraventyBundle\Entity\AgencyEvent
 * 
 * @ORM\Table(name="travel")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\TravelRepository")
 */
class Travel extends Event
{
    /**
     * @var string $date
     *
     * @ORM\Column(name="date", type="datetime", length=255, nullable=true)
     * @Assert\DateTime
     */
    private $date;
    /**
     * @var integer $price
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;
    /**
     * @var integer $attendantLimit
     *
     * @ORM\Column(name="attendantLimit", type="integer")
     * @Assert\Min(limit="0", message="The minimum value is 0")
     */
    private $attendantLimit;
    /**
     * @ORM\ManyToOne(targetEntity="Agency", inversedBy="travels")
     * @ORM\JoinColumn(name="agency_id", referencedColumnName="id")
     */
    private $agency;
    /**
     * @ORM\OneToMany(targetEntity="TravelSubscription", mappedBy="travel", cascade={"all"})
     */
    private $subscriptions;
    
    public function __construct()
    {
        parent::__construct();
        $this->subscriptions = new ArrayCollection();
    }
    
    public function getAgency()
    {
        return $this->agency;
    }

    public function setAgency(Agency $agency = null)
    {
        $this->agency = $agency;
    }

    /**
     * Set date
     *
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set price
     *
     * @param integer $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return integer 
     */
    public function getPrice()
    {
        return $this->price;
    }
    
    /**
     * Set attendantLimit
     *
     * @param integer $attendantLimit
     */
    public function setAttendantLimit($attendantLimit)
    {
        $this->attendantLimit = $attendantLimit;
    }

    /**
     * Get attendantLimit
     *
     * @return integer 
     */
    public function getAttendantLimit()
    {
        return $this->attendantLimit;
    }    

    public function getSubscriptions()
    {
        return $this->subscriptions;
    }

    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }
    
    public function getSubscriptors()
    {
        //$this->travelSubscriptionRepository->findByTravel($this);
        return array();
    }
    
    public function getType()
    {
        return 'TRAVEL';
    }

}
