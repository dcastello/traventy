<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * dcastello\TraventyBundle\Entity\Activity
 *
 * @ORM\Table(name="activity")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\ActivityRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Activity
{
    const DEFAULT_COUNTRY = 'Spain';

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=5,
     *     message="The minimum characters are {{ limit }}"
     * )
     */
    private $name;
    /**
     * @ORM\ManyToOne(targetEntity="City")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;
    /**
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;
    /**
     * @ORM\Column(name="description", type="text")
     * @Assert\NotBlank()
     */
    private $description;
    /**
     * @ORM\Column(name="date", type="datetime", nullable=true)
     * @Assert\DateTime
     */
    private $date;
    /**
     * @ORM\ManyToMany(targetEntity="Traveler", mappedBy="favoritesActivities")
     */
    private $favoritesTravelers;
    /**
     * @ORM\OneToMany(targetEntity="ImageActivity", mappedBy="activity", orphanRemoval=true, cascade={"all"})
     */
    private $images;

    public function __construct()
    {
        $this->favoritesTravelers = new ArrayCollection();
        $this->images = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getName();
    }

    public function getFavoritesTravelers()
    {
        return $this->favoritesTravelers;
    }

    public function addFavoriteTraveler(Traveler $traveler)
    {
        if (!$this->favoritesTravelers->contains($traveler)) {
            $this->favoritesTravelers->add($traveler);
        }
    }

    public function getImages()
    {
        return $this->images;
    }

    public function setImages(Collection $images)
    {
        foreach ($images as $image) {
            $image->setActivity($this);
        }
        $this->images = $images;
    }

    public function addImage(ImageActivity $image)
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->setActivity($this);
        }
    }

    public function removeImage(ImageActivity $image)
    {
        if ($this->images->removeElement($image)) {
            $image->setActivity(null);
        }
    }

    public function getRandomImage()
    {
        $image = $this->getImages()->first();
        if ($image) {
            return $image;
        }
        return null;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set city
     *
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get event date
     *
     * @return datetime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set activity date
     *
     * @param datetime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @ORM\PrePersist
     */
    public function setDefaultValues()
    {
        $this->setCountry(self::DEFAULT_COUNTRY);
    }

}