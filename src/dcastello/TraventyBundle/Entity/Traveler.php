<?php

namespace dcastello\TraventyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * dcastello\TraventyBundle\Entity\Traveler
 * 
 * @ORM\Table(name="traveler")
 * @ORM\Entity(repositoryClass="dcastello\TraventyBundle\Entity\TravelerRepository")
 */
class Traveler extends User
{
    /**
     * @ORM\ManyToMany(targetEntity="Activity", inversedBy="favoritesTravelers")
     * @ORM\JoinTable(name="favorites_activities")
     */
    private $favoritesActivities;
    /**
     * @ORM\ManyToMany(targetEntity="Resource", inversedBy="favoritesTravelers")
     * @ORM\JoinTable(name="favorites_resources")
     */
    private $favoritesResources;
    /**
     * @ORM\OneToMany(targetEntity="Excursion", mappedBy="traveler", cascade={"persist", "remove"})
     */
    private $excursions;
    /**
     * @ORM\OneToMany(targetEntity="TravelSubscription", mappedBy="traveler", cascade={"all"})
     */
    private $subscriptions;
    /**
     * @ORM\Column(name="surname", type="string", length=100) 
     * @Assert\NotBlank()
     * @Assert\MinLength(
     *     limit=3,
     *     message="Your surname must have at least {{ limit }} characters",
     *     groups={"basicInfo"}
     * )
     */
    private $surname;
    /**
     * @ORM\Column(name="birthday", type="datetime", nullable=true) 
     * @Assert\DateTime(groups={"basicInfo"})
     */
    private $birthday;
    /**
     * @ORM\Column(name="nif", type="string", length=9, nullable=true)
     * @Assert\MinLength(
     *     limit=9,
     *     message="Min length 9 characters",
     *     groups={"basicInfo"}
     * )
     */
    private $nif;

    public function __construct()
    {
        $this->favoritesActivities = new ArrayCollection();
        $this->favoritesResources = new ArrayCollection();
        $this->excursions = new ArrayCollection();
        $this->travels = new ArrayCollection();
        $this->subscriptions = new ArrayCollection();
    }
    
    public function __toString()
    {
        return $this->getFullName();
    }
    
    public function getFullName()
    {
        return $this->getName().' '.$this->getSurname();
    }

    /**
     * Set nif
     *
     * @param string $nif
     */
    public function setNif($nif)
    {
        $this->nif = $nif;
    }

    /**
     * Get CIF
     * 
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     *
     * @param string $surname 
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     *
     * @param datetime $birthday 
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getFavoritesActivities()
    {
        return $this->favoritesActivities;
    }

    public function addFavoriteActivity(Activity $activity)
    {
        if (!$this->favoritesActivities->contains($activity)) {
            $activity->addFavoriteTraveler($this);
            $this->favoritesActivities->add($activity);
        }
    }

    public function getFavoritesResources()
    {
        return $this->favoritesResources;
    }

    public function addFavoriteResource(Resource $resource)
    {
        if (!$this->favoritesResources->contains($resource)) {
            $resource->addFavoriteTraveler($this);
            $this->favoritesResources->add($resource);
        }
    }

    public function getExcursions()
    {
        return $this->excursions;
    }

    public function addExcursion(Excursion $excursion)
    {
        if (!$this->excursions->contains($excursion)) {
            $this->excursions->add($excursion);
            $excursion->setTraveler($this);
        }
    }

    public function removeExcursion(Excursion $excursion)
    {
        if ($this->excursions->contains($excursion)) {
            $this->excursions->removeElement($excursion);
            $excursion->setTraveler(null);
        }
    }
    
    public function getSubscriptions()
    {
        return $this->subscriptions;
    }
    
    public function setSubscriptions($subscriptions)
    {
        $this->subscriptions = $subscriptions;
    }    
    
    function getRoles()
    {
        return array('ROLE_TRAVELER');
    }

    function getType()
    {
        return 'TRAVELER';
    }

}
