Traventy
========

### Acerca de Traventy ###

Traventy es un proyecto realizado en Symfony2 por David Castelló. La finalidad del proyecto es el aprendizaje y mejora de los conociemientos en Symfony2.

Instalación y configuración
---------------------------

### Instalación ###

1. `mkdir /var/www/traventy`
2. `git clone git@bitbucket.org:dcastello/traventy.git /var/www/traventy`
3. `cd /var/www/traventy`
4. Ejecutamos el comando `php bin/vendors install` y esperamos a que se instalen todas las librerías externas de Symfony2.
5. `chmod -R 777 app/cache app/logs` (Haz esto más elegantemente [Setting up Permissions](http://symfony.com/doc/current/book/installation.html#configuration-and-setup))

### Configuración de la base de datos ###

Traventy por defecto utiliza una base de datos llamada `traventy` para acceder con el usuario `traventy` cuya contraseña es `traventy`.
Si quieres cambiar estos datos puedes hacer en el fichero `app/config/parameters.ini`

`[parameters]
    database_driver="pdo_mysql"  
    database_host="localhost"  
    database_name="traventy"  
    database_user="traventy"  
    database_password="traventy"`

Una vez configurado el acceso podemos crear la base de datos y todo el esquema de tablas del proyecto.

- `php app/console doctrine:database:create`
- `php app/console doctrine:schema:create`

La aplicación dispone de un conjunto de datos de prueba.

- `php app/console doctrine:fixtures:load`

### Configuración de Apache ###

- Añade la siguiente línea a: `/etc/hosts`
    127.0.0.1   traventy.local

- Crea el nuevo **host** de _traventy_

:::shell
<VirtualHost *:80>
    DocumentRoot   "/var/www/traventy/web"
    DirectoryIndex app.php
    ServerName     traventy.local
    <Directory "/var/www/traventy/web">
        AllowOverride All
        Allow from All
    </Directory>
</VirtualHost>

### Navegando por Traventy local ###

Accede a `http://traventy.local` para el entorno de producción y a `http://traventy.local/app_dev.php` para el entorno de desarrollo.

Prueba Traventy online
----------------------

Si quieres puedes probar **Traventy** antes de descargarlo hazlo visitando [Traventy.com](http://traventy.com).

Usuario **viajero/traveler** de prueba:

- **username**: traveler1@localhost
- **password**: traveler1

Accede a "Account" para gestionar tus **Recursos**, **Actividades** y **Excursiones**. 

### Author ###

David Castelló Alfaro

* [LinkedIn](http://linkedin.com/in/davidcastello)
* [Twitter](https://twitter.com/#!/davidcastello)
* [GitHub](https://github.com/dcastello)
